/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50532
Source Host           : localhost:3306
Source Database       : db_parking

Target Server Type    : MYSQL
Target Server Version : 50532
File Encoding         : 65001

Date: 2013-10-25 07:04:46
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `tbl_deposit`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_deposit`;
CREATE TABLE `tbl_deposit` (
`id_deposit`  varchar(8) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`id_member`  varchar(8) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`tanggal_depo`  date NOT NULL ,
`jumlah`  int(50) NOT NULL ,
`bank_asal`  varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`no_rek`  varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`pemilik_rek`  varchar(35) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`bank_tujuan`  varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`stts`  enum('ok','konfirmasi','pending') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'pending' ,
`userfile`  blob NULL ,
PRIMARY KEY (`id_deposit`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci

;

-- ----------------------------
-- Records of tbl_deposit
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for `tbl_kendaraan`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_kendaraan`;
CREATE TABLE `tbl_kendaraan` (
`id_kendaraan`  int(5) NOT NULL AUTO_INCREMENT ,
`jns_kendaraan`  varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`merk`  varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`tipe`  varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`nopol`  varchar(9) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`warna`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`id_pemilik`  varchar(8) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
PRIMARY KEY (`id_kendaraan`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=3

;

-- ----------------------------
-- Records of tbl_kendaraan
-- ----------------------------
BEGIN;
INSERT INTO `tbl_kendaraan` VALUES ('1', 'motor', 'yamaha', 'mio', 'ab 1234 x', 'pink', 'M-00001'), ('2', 'mobil', 'honda', 'jazz', 'ab 4321 z', 'hitam', 'M-00002');
COMMIT;

-- ----------------------------
-- Table structure for `tbl_member`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_member`;
CREATE TABLE `tbl_member` (
`id_member`  varchar(8) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`no_induk`  varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`username`  varchar(25) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`password`  varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`nm_lengkap`  varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`jurusan`  varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`angkatan`  int(4) NOT NULL ,
`email`  varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`telp`  varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`alamat`  text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`jabatan`  varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`saldo_awal`  varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`saldo_akhir`  varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`stts_hutang`  enum('kosong','ada') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'kosong' ,
`userfile`  blob NOT NULL ,
PRIMARY KEY (`id_member`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci

;

-- ----------------------------
-- Records of tbl_member
-- ----------------------------
BEGIN;
INSERT INTO `tbl_member` VALUES ('M-00001', '08130076', 'Gilang', 'gielank15', 'Gilang Sonar', 'TF', '2008', 'gilangsonar15@gmail.com', '081808066401', 'Kanoman', 'mahasiswa', '100000', '100000', 'kosong', '');
COMMIT;

-- ----------------------------
-- Table structure for `tbl_pelanggaran`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_pelanggaran`;
CREATE TABLE `tbl_pelanggaran` (
`id_pelanggaran`  varchar(8) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`jns_pelanggaran`  varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`tgl_pelanggaran`  date NOT NULL ,
`tgl_tempo`  date NOT NULL ,
`nopol`  varchar(9) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`userfile`  blob NOT NULL ,
`status`  enum('belum lunas','lunas') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'belum lunas' ,
`id_pemilik`  varchar(8) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`denda`  int(10) NOT NULL ,
PRIMARY KEY (`id_pelanggaran`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci

;

-- ----------------------------
-- Records of tbl_pelanggaran
-- ----------------------------
BEGIN;
INSERT INTO `tbl_pelanggaran` VALUES ('ST-00001', 'Mahasiswa Parkir Daerah Tamu', '2013-10-25', '2013-10-30', 'AB 1234 X', 0x696D61676573312E6A7067, 'belum lunas', 'M-00001', '5000');
COMMIT;

-- ----------------------------
-- Auto increment value for `tbl_kendaraan`
-- ----------------------------
ALTER TABLE `tbl_kendaraan` AUTO_INCREMENT=3;
