/**
 * Created with JetBrains PhpStorm.
 * User: Gilang
 * Date: 10/20/13
 * Time: 3:59 AM
 * To change this template use File | Settings | File Templates.
 */
jQuery.noConflict();

jQuery(document).ready(function(){

    // dynamic table
    if(jQuery('#dyntable').length > 0) {
        jQuery('#dyntable').dataTable({
            "sPaginationType": "full_numbers",
            "aaSortingFixed": [[0,'asc']],
            "fnDrawCallback": function(oSettings) {
            }
        });
    }

    jQuery('.tanggal').datepicker();

});
