<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Gilang
 * Date: 10/20/13
 * Time: 3:01 AM
 * To change this template use File | Settings | File Templates.
 */

class Home extends CI_Controller{
    function __construct(){
        parent::__construct();
        if($this->session->userdata('USERNAME') != TRUE && $this->session->userdata('PASSWORD') != TRUE){
            redirect('panel');
        };
    }

    function index(){
        $data=array(
            'title'=>'Parking System - Home',
            'active_home'=> 'active'
        );

        $this->load->view('frontend/element/v_header',$data);
        $this->load->view('frontend/pages/home/v_home');
        $this->load->view('frontend/element/v_footer');
    }
}