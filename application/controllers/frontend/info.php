<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Gilang
 * Date: 10/21/13
 * Time: 11:10 PM
 * To change this template use File | Settings | File Templates.
 */
class Info extends CI_Controller{
    function __construct(){
        parent::__construct();

    }

    function info_pembayaran(){
        $data=array(
            'title'=>'parking System - info pembayaran Denda'
        );
        $this->load->view('frontend/element/v_header',$data);
        $this->load->view('frontend/pages/info/v_info_pembayaran_denda');
        $this->load->view('frontend/element/v_footer');

    }
   function info_deposit(){
       $data=array(
           'title'=>'parking System - info Deposit'
       );
       $this->load->view('frontend/element/v_header',$data);
       $this->load->view('frontend/pages/info/v_info_deposit');
       $this->load->view('frontend/element/v_footer');
   }
    function info_peraturan(){
        $data=array(
            'title'=>'parking System - info peraturan Parkir'
        );
        $this->load->view('frontend/element/v_header',$data);
        $this->load->view('frontend/pages/info/v_info_parkir');
        $this->load->view('frontend/element/v_footer');
    }
    function info_sistem(){
        $data=array(
            'title'=>'parking System - info Deposit'
        );
        $this->load->view('frontend/element/v_header',$data);
        $this->load->view('frontend/pages/info/v_info_sistem');
        $this->load->view('frontend/element/v_footer');
    }
}