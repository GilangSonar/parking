<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Gilang
 * Date: 10/24/13
 * Time: 2:37 PM
 * To change this template use File | Settings | File Templates.
 */

class Setting extends CI_Controller{
    function __construct(){
        parent::__construct();
    }

    function set_account(){
        $data=array(
            'title'=>'Account Settings'
        );
        $this->load->view('frontend/element/v_header',$data);
        $this->load->view('frontend/pages/setting/v_set_account');
        $this->load->view('frontend/element/v_footer');
    }

    function change_pass(){
        $data=array(
            'title'=>'Change Password'
        );
        $this->load->view('frontend/element/v_header',$data);
        $this->load->view('frontend/pages/setting/v_change_pass');
        $this->load->view('frontend/element/v_footer');
    }
}