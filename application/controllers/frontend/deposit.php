<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Gilang
 * Date: 10/20/13
 * Time: 3:01 AM
 * To change this template use File | Settings | File Templates.
 */

class Deposit extends CI_Controller{
    function __construct(){
        parent::__construct();

        $this->load->model('m_deposit');
    }

    function index(){
        $data=array(
            'title'=>'Parking System - Deposit Baru',
            'active_deposit'=>'active',

            'kd_deposit'=>$this->m_deposit->getKodeDeposit(),
        );

        $this->load->view('frontend/element/v_header',$data);
        $this->load->view('frontend/pages/deposit/v_deposit');
        $this->load->view('frontend/element/v_footer');
    }

    function konfirmasi(){
        $data=array(
            'title'=>'Parking System - Konfirmasi',
            'active_deposit'=>'active'
        );

        $this->load->view('frontend/element/v_header',$data);
        $this->load->view('frontend/pages/deposit/v_konfirmasi');
        $this->load->view('frontend/element/v_footer');
    }


}