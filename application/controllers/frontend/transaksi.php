<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Gilang
 * Date: 10/20/13
 * Time: 3:01 AM
 * To change this template use File | Settings | File Templates.
 */

class Transaksi extends CI_Controller{
    function __construct(){
        parent::__construct();
    }

    function index(){
        $data=array(
            'title'=>'Parking System - Transaksi',
            'active_transaksi'=>'active'
        );

        $this->load->view('frontend/element/v_header',$data);
        $this->load->view('frontend/pages/transaksi/v_transaksi');
        $this->load->view('frontend/element/v_footer');
    }

    function bayar_denda(){
        $data=array(
            'title'=>'Parking System - Bayar Denda',
             'active_home'=>'active'
        );

        $this->load->view('frontend/element/v_header',$data);
        $this->load->view('frontend/pages/transaksi/v_bayar_denda');
        $this->load->view('frontend/element/v_footer');
    }
}