<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Gilang
 * Date: 10/24/13
 * Time: 3:51 PM
 * To change this template use File | Settings | File Templates.
 */

class Tilang extends CI_Controller{
    function __construct(){
        parent::__construct();

        $this->load->model('m_tilang');
        $this->load->library('image_lib');
    }

    function buat_st_id(){
        $id=$this->uri->segment(4);
        $data=array(
            'title'=> 'Admin Parking System - Surat Tilang',

            'kd_pelanggaran'=>$this->m_tilang->getKodeTilang(),
            'dt_pelanggar'=>$this->m_tilang->getDataPelanggar($id),
        );

        $this->load->view('backend/element/v_header',$data);
        $this->load->view('backend/pages/tilang/v_buat_st');
        $this->load->view('backend/subelement/v_notifications');
        $this->load->view('backend/element/v_footer');
    }

    function input_st(){

        $config['upload_path'] = './asset/uploads';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size']     = '5000';
        $config['max_width']  = '2000';
        $config['max_height']  = '2000';
        $this->load->library('upload',$config);

        if ( ! $this->upload->do_upload())
        {
            $this->session->set_flashdata('error',true);
        }
        else{
            $file =  $this->upload->data();
            $namaFile = $file['file_name'];
            $data = array(
                'id_pelanggaran'=>$this->input->post('id_pelanggaran'),
                'jns_pelanggaran'=>$this->input->post('jns_pelanggaran'),
                'tgl_pelanggaran'=>date("Y-m-d",strtotime($this->input->post('tgl_pelanggaran'))),
                'tgl_tempo'=>date("Y-m-d",strtotime($this->input->post('tgl_tempo'))),
                'nopol' => $this->input->post('nopol'),
                'status' => "belum lunas",
                'denda' => "5000",
                'id_pemilik' => $this->input->post('id_pemilik'),
                'userfile'=>$namaFile,
            );
            $cek = $this->m_tilang->insertPelanggaran($data);
            if($cek)
            redirect("backend/dashboard");
            $this->session->set_flashdata('success',true);
        }
    }
}