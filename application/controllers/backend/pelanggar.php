<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Gilang
 * Date: 10/24/13
 * Time: 7:47 PM
 * To change this template use File | Settings | File Templates.
 */

class Pelanggar extends CI_Controller{
    function __construct(){
        parent::__construct();
    }

    function index(){
        $data=array(
            'title'=>'Daftar Pelanggar Parkir',
            'active_data_pelanggar'=>'active'
        );
        $this->load->view('backend/element/v_header',$data);
        $this->load->view('backend/pages/pelanggar/v_pelanggar');
        $this->load->view('backend/element/v_footer');
    }
}