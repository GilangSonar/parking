<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Gilang
 * Date: 10/24/13
 * Time: 3:51 PM
 * To change this template use File | Settings | File Templates.
 */

class Dashboard extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('m_kendaraan');
    }

    function index(){
        $data=array(
            'title'=> 'Admin Parking System - Dashboard',
            'active_dashboard'=>'active',
        );

        $this->load->view('backend/element/v_header',$data);
        $this->load->view('backend/pages/dashboard/v_dashboard');
        $this->load->view('backend/element/v_footer');
    }
    
    function tampil(){
        $nopol = $this->input->post('nopol');
        $data['hasil']=$this->m_kendaraan->cek($nopol);
        if($nopol!="")
        {
            foreach($data['hasil']->result() as $result){
                ?>

                <a class="text-hasil" href="<?php echo site_url('backend/tilang/buat_st_id/'.$result->id_pemilik)?>">
                    <?php echo $result->jabatan; ?>
                </a>

            <?php
                }
        }
        else
        {
            echo "ERROR - Input Data Kosong";
        }
    }

}