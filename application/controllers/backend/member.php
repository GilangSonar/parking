<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Gilang
 * Date: 10/24/13
 * Time: 8:27 PM
 * To change this template use File | Settings | File Templates.
 */

class Member extends CI_Controller{
    function __construct(){
        parent::__construct();
        parent::__construct();
//        if($this->session->userdata('NO_INDUK') != TRUE && $this->session->userdata('PASS') != TRUE){
//            redirect('panel');
//        };

        $this->load->model('m_deposit');
        $this->load->model('m_member');
        $this->load->library('image_lib');
    }

//   ========  MEMBER ===========
    function daftar_member(){
        $data=array(
            'title'=>'Daftar Member',
            'active_member'=>'active',

            'dt_member' => $this->m_member->getAllMember(),
            'id_member' => $this->m_member->getKodeMember(),
        );
        $this->load->view('backend/element/v_header',$data);
        $this->load->view('backend/pages/member/v_daftar_member');
        $this->load->view('backend/element/v_footer');
    }

    function add_member(){
        $data=array(
            'title'=>'Add Member',
            'active_member'=>'active',

            'dt_member' => $this->m_member->getAllMember(),
            'id_member' => $this->m_member->getKodeMember(),
        );
        $this->load->view('backend/element/v_header',$data);
        $this->load->view('backend/pages/member/v_add_member');
        $this->load->view('backend/element/v_footer');
    }
    function input_member(){
        $config['upload_path'] = './asset/backend/uploads';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size']     = '5000';
        $config['max_width']  = '2000';
        $config['max_height']  = '2000';
        $this->load->library('upload',$config);


        if ( ! $this->upload->do_upload())
        {
            $data=array(
            'id_member'=>$this->input->post('id_member'),
            'no_induk'=>$this->input->post('no_induk'),
            'username'=>$this->input->post('username'),
            'password'=>md5($this->input->post('password')),
            'nm_lengkap'=>$this->input->post('nm_lengkap'),
            'jurusan'=>$this->input->post('jurusan'),
            'angkatan'=>$this->input->post('angkatan'),
            'email'=>$this->input->post('email'),
            'telp'=>$this->input->post('telp'),
            'jabatan'=>$this->input->post('jabatan'),
            'alamat'=>$this->input->post('alamat'),
            'saldo_awal'=>$this->input->post('saldo_awal'),
        );
        $cek = $this->m_member->insertMember($data);
        if($cek)
            redirect("backend/member/daftar_member");
        }
        else{
            $file =  $this->upload->data();
            $namaFile = $file['file_name'];
            $data=array(
                'id_member'=>$this->input->post('id_member'),
                'no_induk'=>$this->input->post('no_induk'),
                'username'=>$this->input->post('username'),
                'password'=>md5($this->input->post('password')),
                'nm_lengkap'=>$this->input->post('nm_lengkap'),
                'jurusan'=>$this->input->post('jurusan'),
                'angkatan'=>$this->input->post('angkatan'),
                'email'=>$this->input->post('email'),
                'telp'=>$this->input->post('telp'),
                'jabatan'=>$this->input->post('jabatan'),
                'alamat'=>$this->input->post('alamat'),
                'saldo_awal'=>$this->input->post('saldo_awal'),
                'userfile'=>$this->input->post('userfile'),

        );
            $cek = $this->m_member->insertMember($data);
            if($cek)
                redirect("backend/member/daftar_member");
        }
    }


//    ========= DEPOSIT =============
    function deposit_member(){
        $data=array(
            'title'=>'Deposit Member',
            'dt_deposit' => $this->m_deposit->getAllDeposit(),
            'kd_deposit'=>$this->m_deposit->getKodeDeposit(),
            'active_member'=>'active'
        );
        $this->load->view('backend/element/v_header',$data);
        $this->load->view('backend/pages/member/v_deposit_member');
        $this->load->view('backend/element/v_footer');
    }
    function input_deposit(){
        $data=array(
            'id_member'=>$this->input->post('id_member'),
            'id_deposit'=>$this->input->post('id_deposit'),
            'bank_asal'=>$this->input->post('bank_asal'),
            'bank_tujuan'=>$this->input->post('bank_tujuan'),
            'no_rek'=>$this->input->post('no_rek'),
            'pemilik_rek'=>$this->input->post('pemilik_rek'),
            'tanggal_depo'=>date("Y-m-d",strtotime($this->input->post('tanggal'))),
            'jumlah'=>$this->input->post('jumlah'),
            'stts'=>$this->input->post('stts'),
        );
        $cek = $this->m_deposit->insertDeposit($data);
        if($cek)
            redirect("backend/frontend/deposit_member");
    }
}