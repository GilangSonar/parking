<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Gilang
 * Date: 10/24/13
 * Time: 7:47 PM
 * To change this template use File | Settings | File Templates.
 */

class Kendaraan extends CI_Controller{
    function __construct(){
        parent::__construct();
    }

    function index(){
        $data=array(
            'title'=>'form Kendaraan',
            'active_kendaraan'=>'active'
        );
        $this->load->view('backend/element/v_header',$data);
        $this->load->view('backend/pages/kendaraan/v_daftar_kendaraan');
        $this->load->view('backend/element/v_footer');
    }
    function add_kendaraan(){
        $data=array(
            'title'=>'Add Kendaraan',
            'active_kendaraan'=>'active',


        );
        $this->load->view('backend/element/v_header',$data);
        $this->load->view('backend/pages/kendaraan/v_add_kendaraan');
        $this->load->view('backend/element/v_footer');
    }
}