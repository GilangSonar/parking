<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Gilang
 * Date: 10/20/13
 * Time: 3:16 AM
 * To change this template use File | Settings | File Templates.
 */

class Panel extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('m_member');
    }

    function index(){
        $data=array(
            'title' => 'Login Form - Parking System'
        );
        $this->load->view('v_panel',$data);
    }
    function cekLogin() {

        $username = $this->input->post('username');
        $password = md5($this->input->post('password'));
        foreach ($this->m_member->getAllMember() as $r):

            if($username == " " || $password == " ")
            {
                $status = 0;
            }
            elseif ($username != $r->username || $password != $r->password)
            {
                $status = 0;
            }
            else
            {
                $status = 1;
                $data = array(
                    'USERNAME' => $r->username,
                    'PASSWORD' => $r->password,
                    'JABATAN' => $r->jabatan,
                    'ID'=> $r->id_member,

                );
                $this->session->set_userdata($data);
            }

            $output = '{ "status": "'.$status.'" }';
            echo $output;
        endforeach;
    }

    function logout() {
        $this->session->unset_userdata('USERNAME');
        $this->session->unset_userdata('PASSWORD');
        $this->session->unset_userdata('JABATAN');
        $this->session->unset_userdata('ID');

        redirect('panel');
    }
}