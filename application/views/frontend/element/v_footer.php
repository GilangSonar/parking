<div class="container">
    <div class="row text-center" style="padding-bottom: 20px">
        Parking System - Created By :
        <a href="http://facebook.com/gilang.sonar" target="_blank">Gilang Sonar</a> &
        <a href="http://www.facebook.com/alkhoholic" target="_blank">Andreas Hirasna</a>
    </div>
</div>

<!--Basic Core JS-->
<script type="text/javascript" src="<?php echo base_url('asset/js/jquery.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('asset/js/bootstrap.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('asset/js/dataTables.min.js')?>"></script>

<!--Custom JS-->
<script type="text/javascript" src="<?php echo base_url('asset/js/app.js')?>"></script>

</body>
</html>