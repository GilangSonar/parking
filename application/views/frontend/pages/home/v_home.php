<?php $this->load->view('frontend/subelement/v_top')?>
<?php $this->load->view('frontend/subelement/modal/v_modal_foto')?>

<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <strong>ABOUT</strong>
                    <span class="text-danger pull-right"> <strong>Saldo Anda : Rp 100.000</strong></span>
                </h3>

            </div>
            <div class="panel-body">
                <div class="col-xs-12 col-md-3">
                    <div class="thumbnail thumb-user">
                        <img src="<?php echo base_url('asset/img/user.png') ?>" alt="Img"/>
                    </div>
                    <div class="text-center">
                        <h3>John Doe</h3>
                        <h6 class="text-muted">Mahasiswa</h6>
                    </div>
                </div>

                <div class="col-xs-12 col-md-4">
                    <div class="page-header">
                        <h4>Data Diri <small>Mahasiswa</small></h4>
                    </div>
                    <dl class="dl-horizontal">
                        <dt>Nomor Induk</dt>
                        <dd>
                            0123456
                        </dd>
                        <dt>Jurusan</dt>
                        <dd>
                            T.Informatika
                        </dd>
                        <dt>Tahun Angkatan</dt>
                        <dd>
                            2008
                        </dd>
                        <dt>Alamat</dt>
                        <dd>
                            Blok O no 220 <br>
                            Banguntapan, Bantul<br>
                        </dd>
                        <dt>Email</dt>
                        <dd>
                            <a href="mailto:#">user@mail.com</a>
                        </dd>
                        <dt>Telp / HP</dt>
                        <dd>
                            081234455666
                        </dd>
                    </dl>

                </div>

                <div class="col-xs-12 col-md-4">
                    <div class="page-header">
                        <h4>Data Kendaraan <small>Mahasiswa</small></h4>
                    </div>
                    <dl class="dl-horizontal">
                        <dt>Jenis Kendaraan</dt>
                        <dd>
                            Motor
                        </dd>
                        <dt>Merek Kendaraan</dt>
                        <dd>
                            Yamaha
                        </dd>
                        <dt>Tipe Kendaraan</dt>
                        <dd>
                            Mio
                        </dd>
                        <dt>No Polisi</dt>
                        <dd>
                            AB 123 X
                        </dd>
                        <dt>Warna Kendaraan</dt>
                        <dd>
                            Putih
                        </dd>
                    </dl>

                </div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><strong>DATA PELANGGARAN PARKIR</strong></h3>
            </div>

            <div class="panel-body nopadding">
                <table id="dyntable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>No.Surat Tilang</th>
                        <th>Tanggal Tilang</th>
                        <th>Jatuh Tempo</th>
                        <th>Pelanggaran</th>
                        <th>Status</th>
                        <th class="text-center">Pilihan</th>
                    </tr>
                    </thead>
                    <tbody>

                    <tr>
                        <td>1</td>
                        <td>ST-001</td>
                        <td class="text-center" style="vertical-align: middle">
                            <span class="label label-default">10-10-2013</span>
                        </td>
                        <td class="text-center" style="vertical-align: middle">
                            <span class="label label-warning">10-11-2013</span>
                        </td>
                        <td>Mahasiswa Parkir Di Area Dosen &nbsp;
                            <a data-toggle="modal" href="#modalFoto" class="btn btn-default btn-xs">
                                <span class="glyphicon glyphicon-picture"></span>&nbsp; Foto
                            </a>
                        </td>
                        <td class="text-center" style="vertical-align: middle">
                            <span class="label label-success">Lunas</span>
                        </td>
                        <td>
                            <a href="<?php echo site_url('frontend/transaksi/bayar_denda')?>" class="btn btn-primary btn-sm btn-block">
                                <span class="glyphicon glyphicon-hand-right"></span>&nbsp; Bayar Denda
                            </a>
                        </td>
                    </tr>

                    <tr>
                        <td>2</td>
                        <td>ST-002</td>
                        <td class="text-center" style="vertical-align: middle">
                            <span class="label label-default">10-10-2013</span>
                        </td>
                        <td class="text-center" style="vertical-align: middle">
                            <span class="label label-warning">10-11-2013</span>
                        </td>
                        <td>Mahasiswa Parkir Di Area Dosen &nbsp;
                            <a data-toggle="modal" href="#modalFoto" class="btn btn-default btn-xs">
                                <span class="glyphicon glyphicon-picture"></span>&nbsp; Foto
                            </a>
                        </td>
                        <td class="text-center" style="vertical-align: middle">
                            <span class="label label-danger">Belum Lunas</span>
                        </td>
                        <td>
                            <a href="<?php echo site_url('frontend/transaksi/bayar_denda')?>" class="btn btn-primary btn-sm btn-block">
                                <span class="glyphicon glyphicon-hand-right"></span>&nbsp; Bayar Denda
                            </a>
                        </td>
                    </tr>

                    <tr>
                        <td>3</td>
                        <td>ST-003</td>
                        <td class="text-center" style="vertical-align: middle">
                            <span class="label label-default">10-10-2013</span>
                        </td>
                        <td class="text-center" style="vertical-align: middle">
                            <span class="label label-warning">10-11-2013</span>
                        </td>
                        <td>Mahasiswa Parkir Di Area Dosen &nbsp;
                            <a data-toggle="modal" href="#modalFoto" class="btn btn-default btn-xs">
                                <span class="glyphicon glyphicon-picture"></span>&nbsp; Foto
                            </a>
                        </td>
                        <td class="text-center" style="vertical-align: middle">
                            <span class="label label-danger">Belum Lunas</span>
                        </td>
                        <td>
                            <a href="<?php echo site_url('frontend/transaksi/bayar_denda')?>" class="btn btn-primary btn-sm btn-block">
                                <span class="glyphicon glyphicon-hand-right"></span>&nbsp; Bayar Denda
                            </a>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div> <!-- /container -->