<?php $this->load->view('frontend/subelement/v_top')?>

<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <strong>DETAIL PEMBAYARAN DENDA</strong>
                    <span class="text-danger pull-right"> <strong>Saldo Anda : Rp 100.000</strong></span>
                </h3>

            </div>
            <div class="panel-body">

                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Tanggal Pembayaran</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" placeholder="D-001" readonly="readonly">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Kode Pembayaran</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" placeholder="D-001" readonly="readonly">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">No Surat Tilang</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" placeholder="D-001" readonly="readonly">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Tanggal Pelanggaran</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" placeholder="D-001" readonly="readonly">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Jumlah Denda</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" placeholder="Rp. 10.000 ">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Kode Verifikasi</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" placeholder="Masukan Kode Veifikasi Diatas"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button type="submit" class="btn btn-default">Bayar Denda Sekarang</button>
                            <a href="<?php echo site_url('frontend/home') ?>" class="btn btn-default"> Batal</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div> <!-- /container -->