<?php $this->load->view('frontend/subelement/v_top')?>
<?php $this->load->view('frontend/subelement/modal/v_modal_detail_transaksi')?>

<div class="container">

    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><strong>DATA TRANSAKSI</strong></h3>
            </div>

            <div class="panel-body nopadding">
                <table id="dyntable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Kode Transaksi</th>
                        <th>Tanggal</th>
                        <th>Jenis Transaksi</th>
                        <th>Jumlah</th>
                        <th>Saldo Awal</th>
                        <th>Saldo Akhir</th>
                        <th>Pilihan</th>
                    </tr>
                    </thead>
                    <tbody>

                    <tr>
                        <td>1</td>
                        <td>D-001</td>
                        <td>10-11-2013</td>
                        <td class="text-center" style="vertical-align: middle">
                            <span class="label label-success">Deposit</span>
                        </td>
                        <td>Rp. 10.000</td>
                        <td>Rp. 100.000</td>
                        <td>Rp. 110.000</td>
                        <td>
                            <button type="button" data-toggle="modal" data-target="#modalDetailTransaksi" class="btn btn-primary btn-sm btn-block">
                                <span class="glyphicon glyphicon-search"></span>&nbsp; Detail
                            </button>
                        </td>
                    </tr>

                    <tr>
                        <td>1</td>
                        <td>B-001</td>
                        <td>15-11-2013</td>
                        <td class="text-center" style="vertical-align: middle">
                            <span class="label label-danger">Bayar Denda</span>
                        </td>
                        <td>Rp. 10.000</td>
                        <td>Rp. 110.000</td>
                        <td>Rp. 100.000</td>
                        <td>
                            <button type="button" data-toggle="modal" data-target="#modalDetailTransaksi" class="btn btn-primary btn-sm btn-block">
                                <span class="glyphicon glyphicon-search"></span>&nbsp; Detail
                            </button>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>


</div> <!-- /container -->