<?php $this->load->view('frontend/subelement/v_top')?>

<div class="container">

    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><strong>CHANGE PASSWORD</strong></h3>
            </div>

            <div class="panel-body">
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Password Lama</label>
                        <div class="col-lg-10">
                            <input type="password" class="form-control" placeholder="Masukan Password Lama Anda">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Password Baru</label>
                        <div class="col-lg-10">
                            <input type="password" class="form-control" placeholder="Masukan Password Baru Anda">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Konfirmasi Password Baru</label>
                        <div class="col-lg-10">
                            <input type="password" class="form-control" placeholder="Konfirmasi Password Baru">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button type="submit" class="btn btn-default">Update Password</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div> <!-- /container -->