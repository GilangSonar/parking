<?php $this->load->view('frontend/subelement/v_top')?>

<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <strong>ACCOUNT SETTINGS</strong>
                </h3>

            </div>
            <div class="panel-body">
                <div class="col-xs-12 col-md-3">
                    <div class="thumbnail thumb-user">
                        <img src="<?php echo base_url('asset/img/user.png') ?>" alt="Img"/>
                    </div>
                    <div class="text-center">
                        <h3>John Doe</h3>
                        <h6 class="text-muted">Mahasiswa</h6>
                    </div>
                </div>

                <div class="col-xs-12 col-md-9">
                    <form class="form-horizontal" role="form">

<!--                        ========= BIODATA ============-->

                        <div class="page-header">
                            <h4>Biodata <small>Mahasiswa</small></h4>
                        </div>
                        <input type="hidden" name="nim">

                        <div class="form-group">
                            <label class="col-lg-2 control-label">No Induk Mahasiswa</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control"  readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Jurusan</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Tahun Angkatan</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control"readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Alamat</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Email</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Telp / HP</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-2 control-label">Foto</label>
                            <div class="col-lg-10">
                                <input type="file">
                                <p class="help-block">Format : PDF / JPG</p>
                            </div>
                        </div>

                        <hr>

<!--                   ========= DATA KENDARAAN ============-->

                        <div class="page-header">
                            <h4>Data Kendaraan <small>Mahasiswa</small></h4>
                        </div>

                        <div class="form-group">
                        <label class="col-lg-2 control-label">Jenis Kendaraan</label>
                        <div class="col-lg-10">
                            <select class="form-control">
                                <option>Mobil</option>
                                <option>Motor</option>
                            </select>
                        </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-2 control-label">Merk Kendaraan</label>
                            <div class="col-lg-10">
                                <select class="form-control">
                                    <option>Yamaha</option>
                                    <option>Honda</option>
                                    <option>Suzuki</option>
                                    <option>Kawazaki</option>
                                </select>
                            </div>
                         </div>

                        <div class="form-group">
                            <label class="col-lg-2 control-label">Tipe Kendaraan</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">No Polisi</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Warna Kendaraan</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" >
                            </div>
                        </div>

<!--                    ------------------------------------------------------------>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button type="submit" class="btn btn-default">Update Data</button>
                            </div>
                        </div>
                    </form>

                </div>

            </div>
        </div>

    </div>

</div> <!-- /container -->