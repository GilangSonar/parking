<?php $this->load->view('frontend/subelement/v_top')?>
<?php $this->load->view('frontend/subelement/modal/v_modal_detail_deposit')?>

<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <strong>FORM DEPOSIT BARU</strong>
                    <span class="text-danger pull-right"> <strong>Saldo Anda : Rp 100.000</strong></span>
                </h3>

            </div>
            <div class="panel-body">

                <form class="form-horizontal" role="form" method="post" action="<?php echo site_url('backend/member/input_deposit')?>">

                    <input type="hidden" value="M-000001" name="id_member"/>
                    <input type="hidden" value="pending" name="stts"/>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Tanggal</label>
                        <div class="col-lg-10">
                            <input id="tanggal" type="text" name="tanggal" class="form-control" readonly data-date-format="dd-mm-yyyy" data-date="12-02-2012" value="<?php echo date('d-m-Y')?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Kode Deposit</label>
                        <div class="col-lg-10">
                            <input type="text" name="id_deposit" class="form-control" value="<?php echo $kd_deposit?>" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Bank Asal</label>
                        <div class="col-lg-10">
                            <select class="form-control" name="bank_asal">
                                <option value="bca">BCA</option>
                                <option value="bni">BNI</option>
                                <option value="bri">BRI</option>
                                <option value="mandiri">MANDIRI</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">No Rekening</label>
                        <div class="col-lg-10">
                            <input name="no_rek" type="text" class="form-control" placeholder="No. Rekening">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Pemilik Rekening</label>
                        <div class="col-lg-10">
                            <input name="pemilik_rek" type="text" class="form-control" placeholder="Pemilik Rekening">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Jumlah Deposit</label>
                        <div class="col-lg-10">
                            <select class="form-control" name="jumlah">
                                <option value="50000">Rp. 50.000</option>
                                <option value="100000">Rp. 100.000</option>
                                <option value="150000">Rp. 150.000</option>
                                <option value="200000">Rp. 200.000</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Bank Tujuan</label>
                        <div class="col-lg-10">
                            <select class="form-control" name="bank_tujuan">
                                <option value="bca">BCA ( Atas Nama : STTA )</option>
                                <option value="bni">BNI ( Atas Nama : STTA )</option>
                                <option value="bri">BRI ( Atas Nama : STTA )</option>
                                <option value="mandiri">MANDIRI ( Atas Nama : STTA )</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button type="submit" class="btn btn-default">Send</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>

    </div>

    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><strong>DATA DEPOSIT ANDA</strong></h3>
            </div>

            <div class="panel-body nopadding">
                <table id="dyntable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Kode Deposit</th>
                        <th>Tanggal</th>
                        <th>Bank Asal</th>
                        <th>Bank Tujuan</th>
                        <th>Jumlah</th>
                        <th>Status</th>
                        <th class="text-center">Pilihan</th>
                    </tr>
                    </thead>
                    <tbody>

                    <tr>
                        <td>1</td>
                        <td>D-001</td>
                        <td>10-11-2013</td>
                        <td>BCA</td>
                        <td>BCA</td>
                        <td>Rp. 10.000</td>
                        <td class="text-center" style="vertical-align: middle">
                            <span class="label label-warning">Pending</span>
                        </td>
                        <td class="text-center">
                            <div class="btn-group btn-group-sm">
                                <a class="btn btn-primary" href="#modalDetailDeposit" data-toggle="modal" >Detail</a>
                                <a class="btn btn-success" href="<?php echo site_url('frontend/deposit/konfirmasi')?>">Konfirmasi</a>
                                <a class="btn btn-danger" href="#">Cancel</a>

                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td>1</td>
                        <td>D-002</td>
                        <td>10-11-2013</td>
                        <td>BCA</td>
                        <td>BCA</td>
                        <td>Rp. 10.000</td>
                        <td class="text-center" style="vertical-align: middle">
                            <span class="label label-success">Ok</span>
                        </td>
                        <td class="text-center">
                            <div class="btn-group btn-group-sm">
                                <a class="btn btn-primary"  href="#modalDetailDeposit" data-toggle="modal">Detail</a>
                                <a class="btn btn-success" href="<?php echo site_url('frontend/deposit/konfirmasi')?>">Konfirmasi</a>
                                <a class="btn btn-danger" href="#">Cancel</a>

                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>D-003</td>
                        <td>10-11-2013</td>
                        <td>BCA</td>
                        <td>BCA</td>
                        <td>Rp. 10.000</td>
                        <td class="text-center" style="vertical-align: middle">
                            <span class="label label-danger">Cancel</span>
                        </td>
                        <td class="text-center">
                            <div class="btn-group btn-group-sm">
                                <a class="btn btn-primary"  href="#modalDetailDeposit" data-toggle="modal">Detail</a>
                                <a class="btn btn-success" href="<?php echo site_url('frontend/deposit/konfirmasi')?>">Konfirmasi</a>
                                <a class="btn btn-danger" href="#">Cancel</a>
                            </div>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div> <!-- /container -->