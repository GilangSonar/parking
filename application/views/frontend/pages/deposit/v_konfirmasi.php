<?php $this->load->view('frontend/subelement/v_top')?>

<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <strong>FORM KONFIRMASI PEMBAYARAN</strong>
                    <span class="text-danger pull-right"> <strong>Saldo Anda : Rp 100.000</strong></span>
                </h3>

            </div>
            <div class="panel-body">

                <form class="form-horizontal" role="form">

                    <input type="hidden" name="nim">

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Kode Konfirmasi</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" placeholder="K-001" readonly="readonly">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Kode Deposit</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" placeholder="D-001" readonly="readonly">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Bank Asal</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control"readonly="readonly">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Pemilik Rekening</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" readonly="readonly">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Bank Tujuan</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" readonly="readonly">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Jumlah</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" readonly="readonly">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Bukti Transfer Pembayaran</label>
                        <div class="col-lg-10">
                            <input type="file">
                            <p class="help-block">Format : PDF / JPG</p>
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Kode Verifikasi</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" placeholder="Masukan Kode Veifikasi Diatas"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button type="submit" class="btn btn-default">Send</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div> <!-- /container -->