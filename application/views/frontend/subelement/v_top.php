<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Parking System</a>
        </div>
        <div class="navbar-collapse collapse">

            <ul class="nav navbar-nav">
                <li class="<?php if(isset($active_home)){echo $active_home;} ?>"><a href="<?php echo site_url('frontend/home')?>">Home</a></li>
                <li class="<?php if(isset($active_transaksi)){echo $active_transaksi;} ?>"><a href="<?php echo site_url('frontend/transaksi')?>">Data Transaksi</a></li>
                <li class="<?php if(isset($active_deposit)){echo $active_deposit;} ?>"><a href="<?php echo site_url('frontend/deposit')?>">Deposit</a></li>
            </ul>

            <ul class="nav navbar-nav  navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Info <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li class="dropdown-header"><strong>User Guide</strong></li>
                        <li><a href="<?php echo site_url('frontend/info/info_pembayaran')?>"> Cara Pembayaran</a></li>
                        <li><a href="<?php echo site_url('frontend/info/info_deposit')?>">Cara Deposit</a></li>
                        <li class="divider"></li>
                        <li><a href="<?php echo site_url('frontend/info/info_peraturan')?>">Peraturan Parkir Kampus</a></li>
                        <li><a href="<?php echo site_url('frontend/info/info_sistem')?>">Tentang Sistem</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Username <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo site_url('frontend/setting/set_account')?>"><span class="glyphicon glyphicon-user"></span> Account Setting</a></li>
                        <li><a href="<?php echo site_url('frontend/setting/change_pass')?>"><span class="glyphicon glyphicon-cog"></span> Change Password</a></li>
                        <li class="divider"></li>
                        <li><a href="<?php echo base_url()?>"><span class="glyphicon glyphicon-log-out"></span> Sign Out</a></li>
                    </ul>
                </li>
            </ul>

        </div><!--/.nav-collapse -->
    </div>
</div>