<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?php echo base_url('asset/img/logo.png')?>">

    <title><?php echo $title; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('asset/css/bootstrap.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('asset/css/bootstrap-theme.css')?>" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url('asset/css/signin.css')?>" rel="stylesheet">
    <script type="text/javascript" src="<?php echo base_url('asset/js/jquery.js')?>"></script>
    <script type="text/javascript">
        $(function(){
            $("#tombol").click(function() {
                var username=$("#username").val();
                var password=$("#password").val();
                $.ajax({
                    type: "POST",
                    url: "<?php echo site_url('panel/cekLogin')?>",
                    dataType: "text",
                    data: "username="+username+"&password="+password,
                    cache:false,
                    success: function(data){
                        $(".alert-login").fadeIn(1500).fadeOut(2000)
                            .queue(function(){
                                window.location = "<?php echo site_url('frontend/home') ?>";
                            });
                    }
                });
                return false;
            });

        });
    </script>
</head>

<body>

<div class="container">

    <form id="loginform" action="" method="post" class="form-signin">
        <div class="thumbnail">
            <img src="<?php echo base_url('asset/img/logo.png') ?>" alt="Logo"/>
        </div>


        <h2 class="form-signin-heading text-center">Parking System</h2>
        <h5 class="text-center">Sign In with your NIM</h5>
        <div class="center alert-login" style="display: none">
            <div class="progress progress-info progress-striped active">
                <div class="bar" style="width: 100%;"></div>
            </div>
        </div>
        <input id="username" type="text" name="username" class="form-control" placeholder="Username" autofocus>
        <input id="password" type="password" name="password" class="form-control" placeholder="Password">

<!--        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>-->
        <button  id="tombol" class="btn btn-lg btn-primary btn-block">Sign in</button>
    </form>

</div>

</body>
</html>