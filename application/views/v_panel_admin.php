<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?php echo base_url('asset/img/logo.png')?>">

    <title><?php echo $title; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('asset/css/bootstrap.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('asset/css/bootstrap-theme.css')?>" rel="stylesheet">
    <script type="text/javascript" src="<?php echo base_url('asset/js/jquery.js')?>"></script>

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url('asset/css/signin.css')?>" rel="stylesheet">
    <script type="text/javascript">
        $(function(){
            $("#tombol-submit").click(function() {
                var username=$("#username").val();
                var password=$("#password").val();
                $.ajax({
                    type: "POST",
                    url: "<?php echo site_url('panel/cekLogin')?>",
                    dataType: "text",
                    data: "username="+username+"&password="+password,
                    cache:false,
                    success: function(data){
                        $(".alert-login").fadeIn(1500).fadeOut(2000)
                            .queue(function(){
                                window.location = "<?php echo site_url('backend/dashboard') ?>";
                            });
                    }
                });
                return false;
            });

        });
    </script>
</head>

<body>

<div class="container">

    <form class="form-signin">
        <div class="thumbnail">
            <img src="<?php echo base_url('asset/img/logo.png') ?>" alt="Logo"/>
        </div>
        <h2 class="form-signin-heading text-center">Administrator <br> Parking System</h2>
        <h5 class="text-center">Sign In with admin account</h5>
        <form class="form-signin" id="loginform" action="#" method="post">
            <input id="username" type="text" class="form-control" placeholder="Username" autofocus>
            <input id="password" type="password" class="form-control" placeholder="Password">



<!--        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>-->
        <a id="tombol-submit" href="" class="btn btn-lg btn-primary btn-block" type="submit">Sign in</a>
            </form>
    </form>

</div>

</body>
</html>