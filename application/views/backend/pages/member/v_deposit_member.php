<?php $this->load->view('backend/subelement/v_top')?>

<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <strong>DAFTAR DEPOSIT MEMBER</strong>
                </h3>

            </div>
            <div class="panel-body">
                <table id="dyntable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Kode Deposit</th>
                        <th>Tanggal</th>
                        <th>Username</th>
                        <th>Jumlah</th>
                        <th>Bank Asal</th>
                        <th>Bank Tujuan</th>
                        <th>Status</th>
                        <th>Pilihan</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    $no=1;
                    if(isset($dt_deposit)){
                    foreach($dt_deposit as $row){
                    ?>

                    <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $row->id_deposit; ?></td>
                        <td><?php echo $row->tanggal_depo; ?></td>
                        <td><?php echo $row->username; ?></td>
                        <td><?php echo $row->jumlah; ?></td>
                        <td><?php echo $row->bank_asal; ?></td>
                        <td><?php echo $row->bank_tujuan; ?></td>

                        <?php if ($row->stts == "pending") {?>
                            <td class="text-center" style="vertical-align: middle">
                                <span class="label label-warning">Pending</span>
                            </td>

                        <?php }elseif ($row->stts == "konfirmasi")
                        { ?>
                            <td class="text-center" style="vertical-align: middle">
                                <span class="label label-info">Konfirmasi</span>
                            </td>

                        <?php }else {?>
                            <td class="text-center" style="vertical-align: middle">
                                <span class="label label-success">Ok</span>
                            </td>
                        <?php } ?>

                        <td>
                            <button type="button" data-toggle="modal" data-target="#modalDetailTransaksi" class="btn btn-primary btn-sm btn-block">
                                <span class="glyphicon glyphicon-search"></span>&nbsp; Detail
                            </button>
                        </td>
                    </tr>

                    <?php }
                    }
                    ?>

                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div> <!-- /container -->