<?php $this->load->view('backend/subelement/v_top')?>

<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <strong>FORM ADD MEMBER</strong>
                </h3>

            </div>
            <div class="panel-body">
                <form class="form-horizontal" role="form" method="post" action="<?php echo site_url("backend/member/input_member")?>" enctype="multipart/form-data">

                    <input type="hidden" name="nim">
                    <div class="page-header">
                        <h3>Pemilik</h3>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label" for="id_member">No Member</label>
                        <div class="col-lg-10">
                            <input type="text" name="id_member" id="id_member" class="form-control" value="<?php echo $id_member;?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label" for="id_induk">No Identitas NIP/NIM :</label>
                        <div class="col-lg-10">
                            <input type="text" name="no_induk" id="no_induk" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label" for="nm_lengkap">Nama Lengkap :</label>
                        <div class="col-lg-10">
                            <input type="text" name="nm_lengkap" id="nm_lengkap" class="form-control" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label" for="username">Username :</label>
                        <div class="col-lg-10">
                            <input type="text" name="username" id="username" class="form-control" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label" for="password">Password :</label>
                        <div class="col-lg-10">
                            <input type="password" name="password" id="password" class="form-control" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label" for="jurusan">Jurusan :</label>
                        <div class="col-lg-10">
                            <select  name="jurusan" id="jurusan" class="form-control">
                                <option value="teknik_informatika">Teknik Informatika</option>
                                <option value="teknik_penerbangan">Teknik Penerbangan</option>
                                <option value="teknik_mesin">Teknik Mesin</option>
                                <option value="teknik_industri">Teknik Industri</option>
                                <option value="teknik_elektro">Teknik Elektro</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label" for="angkatan">Tahun Angkatan :</label>
                        <div class="col-lg-10">
                            <select class="form-control" name="angkatan" id="angkatan">
                                <option value="2001">2001</option>
                                <option value="2002">2002</option>
                                <option value="2003">2003</option>
                                <option value="2004">2004</option>
                                <option value="2005">2005</option>
                                <option value="2006">2006</option>
                                <option value="2007">2007</option>
                                <option value="2008">2008</option>
                                <option value="2009">2009</option>
                                <option value="2010">2010</option>
                                <option value="2011">2011</option>
                                <option value="2012">2012</option>
                                <option value="2013">2013</option>
                                <option value="2014">2014</option>
                                <option value="2015">2015</option>
                                <option value="2016">2016</option>
                                <option value="2017">2017</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label" for="alamat">Alamat :</label>
                        <div class="col-lg-10">
                            <input type="text" name="alamat"  id="alamat" class="form-control" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label" for="email">Email :</label>
                        <div class="col-lg-10">
                            <input type="email" name="email" id="email" class="form-control" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label" for="telp">Telp / HP :</label>
                        <div class="col-lg-10">
                            <input type="text" name="telp" id="telp" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label" for="jabatan">Jabatan :</label>
                        <div class="col-lg-10">
                            <select class="form-control" name="jabatan" id="jabatan">
                                <option value="admin">Admin</option>
                                <option value="dosen">Dosen</option>
                                <option value="mahasiswa">Mahasiswa</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label" for="saldo_awal">Saldo :</label>
                        <div class="col-lg-10">
                            <input type="text" name="saldo_awal" id="saldo_awal" class="form-control" placeholder="100000">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label" for="userfile">Upload Photo :</label>
                        <div class="col-lg-10">
                            <input type="file" name="userfile" id="userfile" class="">
                            <p class="help-block ">Format : PDF / JPG</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button type="submit" class="btn btn-default">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div> <!-- /container -->