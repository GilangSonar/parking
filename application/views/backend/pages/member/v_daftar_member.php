<?php $this->load->view('backend/subelement/v_top')?>

<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <strong>DAFTAR MEMBER</strong>
                </h3>

            </div>
            <div class="panel-body">
                <a href="<?php echo site_url('backend/member/add_member')?>" class="btn btn-primary pull-right">+ Tambah Member</a><br><br>
                <table id="dyntable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th class="text-center">Nomor Identitas</th>
                        <th class="text-center">Nama</th>
                        <th class="text-center">Saldo</th>
                        <th class="text-center">Jabatan</th>
                        <th class="text-center">Photo</th>
                        <th class="text-center">Pilihan</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no = 1;
                    if(isset($dt_member)){
                    foreach($dt_member as $row){
                    ?>
                    <tr>
                        <td class="text-center" style="vertical-align: middle"><?php echo $no++; ?></td>
                        <td   class="text-center" style="vertical-align: middle"><?php echo $row->no_induk;?></td>
                        <td  class="text-center" style="vertical-align: middle"><?php echo $row->username;?></td>
                        <td  class="text-center" style="vertical-align: middle">Rp.<?php echo $row->saldo_awal;?></td>
                        <td class="text-center" style="vertical-align: middle">
                            <span class="label label-success"><?php echo $row->jabatan;?></span>
                        </td>
                        <td class="text-center" style="vertical-align: middle">
                            <img class="img-thumbnail" src="<?php echo base_url('asset/backend/uploads/'.$row->userfile)?>" alt="Foto Upload">
                        </td>
                        <td style="vertical-align: middle">
                            <button type="button" data-toggle="modal" data-target="#modalDetailTransaksi" class="btn btn-primary btn-sm btn-block ">
                                <span class="glyphicon glyphicon-search"></span>&nbsp; Detail
                            </button>
                        </td>
                    </tr>
                    <?php }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div> <!-- /container -->