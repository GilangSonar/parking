<script type="text/javascript" src="<?php echo base_url('asset/js/jquery.pack.js')?>"></script>
<script type="text/javascript">
    $(function(){
        $("#tombol-cek").click(function() {
            $('#loading').ajaxStart(function(){
                $(this).fadeIn();
            }).ajaxStop(function(){
                    $(this).fadeOut();
                });

            var nopol=$("#nopol").val();
            $.ajax({
                type: "POST",
                url: "<?php echo site_url()?>backend/dashboard/tampil",
                dataType: "text",
                data: "nopol="+nopol,
                cache:false,
                success: function(data){
                    if(data.length >0) {
                        $('#dataPencarian').html(data);
                    }
                }
            });
            return false;
        });

    });
</script>

<?php $this->load->view('backend/subelement/v_top')?>

<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <strong>CEK NOMOR POLISI</strong>
                </h3>

            </div>
            <div class="panel-body">
                <form action="#">
                    <div class="jumbotron text-center">

                        <div id="loading" style="display:none">
                            <img src="<?php echo base_url('asset/img/loading.gif'); ?>" width="20px">
                        </div>

                        <h1 id="dataPencarian">Input Nomor Polisi</h1>
                        <h4 class="text-muted">Inputkan nomor polisi kendaraan tanpa spasi</h4>
                        <p>
                            <input id="nopol" type="text" class="input-lg-check form-control" autofocus>
                        </p>
                        <p>
                            <button id="tombol-cek" class="btn btn-lg btn-primary">Check Now !</button>
                        </p>
                        <p>
                            <button type="reset" class="btn btn-default">RESET</button>
                        </p>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div> <!-- /container -->