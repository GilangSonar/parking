<?php $this->load->view('backend/subelement/v_top')?>

<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <strong>FORM ADD MEMBER</strong>
                </h3>

            </div>
            <div class="panel-body">
                <form class="form-horizontal" role="form" method="post" action="<?php echo site_url("backend/member/input_member")?>" enctype="multipart/form-data">

                    <input type="hidden" name="nim">
                    <div class="page-header">
                        <h3>Pemilik</h3>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label" for="id_member">No Member</label>
                        <div class="col-lg-10">
                            <input type="text" name="id_member" id="id_member" class="form-control" value="" readonly="readonly">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label" for="id_induk">No Kendaraan :</label>
                        <div class="col-lg-10">
                            <input type="text" name="no_induk" id="no_induk" class="form-control" readonly="readonly">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label" for="nm_lengkap">Jenis Kendaraan :</label>
                        <div class="col-lg-10">
                            <select  name="jurusan" id="jurusan" class="form-control">
                                <option value="mobil">Mobil</option>
                                <option value="motor">Motor</option>

                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label" for="username">Merk :</label>
                        <div class="col-lg-10">
                            <input type="text" name="merk" id="merk" class="form-control" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label" for="password">Tipe :</label>
                        <div class="col-lg-10">
                            <input type="text" name="tipe" id="tipe" class="form-control" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label" for="jurusan">Nopol :</label>
                        <div class="col-lg-10">
                            <input type="text" name="nopol"  id="nopol" class="form-control" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label" for="angkatan">Warna  :</label>
                        <div class="col-lg-10">
                            <input type="text" name="warna"  id="warna" class="form-control" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label" for="alamat">Alamat :</label>
                        <div class="col-lg-10">
                            <input type="text" name="alamat"  id="alamat" class="form-control" placeholder="">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button type="submit" class="btn btn-default">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div> <!-- /container -->