<?php $this->load->view('backend/subelement/v_top')?>
<?php $this->load->view('backend/subelement/modal/v_modal_kendaraan')?>
<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <strong>DAFTAR MEMBER</strong>
                </h3>

            </div>
            <div class="panel-body">
                <a href="<?php echo site_url('backend/kendaraan/add_kendaraan')?>" class="btn btn-primary pull-right">+ Tambah Kendaraan</a><br><br>
                <table id="dyntable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th class="text-center">Id_pemilik</th>
                        <th class="text-center">Id_kendaraan</th>
                        <th class="text-center">Jenis Kendaraan</th>
                        <th class="text-center">Merk</th>
                        <th class="text-center">Tipe</th>
                        <th class="text-center">nopol</th>
                        <th class="text-center">warna</th>
                        <th class="text-center">action</th>
                    </tr>
                    </thead>
                    <tbody>

                            <tr>
                                <td class="text-center" style="vertical-align: middle">1</td>
                                <td class="text-center" style="vertical-align: middle">PM-1212</td>
                                <td class="text-center" style="vertical-align: middle">asda</td>
                                <td class="text-center" style="vertical-align: middle">asda</td>
                                <td class="text-center" style="vertical-align: middle">asdasd</span></td>
                                <td class="text-center" style="vertical-align: middle">asdad</span></td>
                                <td class="text-center" style="vertical-align: middle">asda</span></td>
                                <td class="text-center" style="vertical-align: middle">adaw</span></td>
                                <td style="vertical-align: middle">
                                    <button type="button" data-toggle="modal" data-target="#modalDetailKendaraan" class="btn btn-primary btn-sm btn-block ">
                                        <span class="glyphicon glyphicon-search"></span>&nbsp; Detail
                                    </button>
                                </td>
                            </tr>

                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div> <!-- /container -->