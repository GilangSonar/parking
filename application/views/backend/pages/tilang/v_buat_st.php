<?php $this->load->view('backend/subelement/v_top')?>

<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <strong>FORM SURAT TILANG</strong>
                </h3>

            </div>
            <div class="panel-body">
                <?php
                if(isset($dt_pelanggar)) {
                foreach($dt_pelanggar as $row) {
                ?>

                <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo site_url('backend/tilang/input_st')?>">
                    <div class="form-group">
                        <label class="col-lg-2 control-label">No Surat Tilang</label>
                        <div class="col-lg-10">
                            <input name="id_pelanggaran" type="text" class="form-control" value="<?php echo $kd_pelanggaran; ?>" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Nopol Kendaraan</label>
                        <div class="col-lg-10">
                            <input name="nopol" type="text" class="form-control" value="<?php echo strtoupper($row->nopol) ?>" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Pemilik Kendaraan</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" value="<?php echo $row->nm_lengkap; ?>" readonly>
                            <input type="hidden" name="id_pemilik" value="<?php echo $row->id_pemilik; ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Tanggal tilang</label>
                        <div class="col-lg-10">
                            <input type="text" name="tgl_pelanggaran" class="form-control" readonly data-date-format="dd-mm-yyyy" data-date="12-02-2012" value="<?php echo date('d-m-Y')?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Tanggal jatuh tempo</label>
                        <div class="col-lg-10">
                            <input type="text" name="tgl_tempo" class="form-control tanggal" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Jenis Pelanggaran</label>
                        <div class="col-lg-10">
                            <select class="form-control" name="jns_pelanggaran">
                                <option value="Mahasiswa Parkir Daerah Dosen">Mahasiswa Parkir Daerah Dosen</option>
                                <option value="Mahasiswa Parkir Daerah Tamu">Mahasiswa Parkir Daerah Tamu</option>
                                <option value="Dosen Parkir Daerah Mahasiswa">Dosen Parkir Daerah Mahasiswa</option>
                                <option value="Dosen Parkir Daerah Tamu">Dosen Parkir Daerah Tamu</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Upload Foto Bukti</label>
                        <div class="col-lg-10">
                            <input type="file" name="userfile">
                            <p class="help-block">Format : PDF / JPG</p>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button type="submit" class="btn btn-default">Send</button>
                        </div>
                    </div>
                </form>

                <?php }
                }
                ?>
            </div>
        </div>

    </div>
</div> <!-- /container -->