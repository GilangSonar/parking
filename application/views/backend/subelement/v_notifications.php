<?php if ($this->session->flashdata('success')) : ?>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery.gritter.add({
                title:	'Sukses',
                text:	'Berhasil menambah data' ,
                image: 	'<?php echo base_url('/asset/backend/img/notif_success.png') ?>',
                sticky: false
            });
        });
    </script>
<?php endif ?>
<?php if ($this->session->flashdata('success_update')) : ?>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery.gritter.add({
                title:	'Update Berhasil',
                text:	'Berhasil update data' ,
                image: 	'<?php echo base_url('/asset/backend/img/notif_success.png') ?>',
                sticky: false
            });
        });
    </script>
<?php endif ?>
<?php if ($this->session->flashdata('success_sent')) : ?>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery.gritter.add({
                title:	'kirim Berhasil',
                text:	'Berhasil mengirim' ,
                image: 	'<?php echo base_url('/asset/backend/img/notif_success.png') ?>',
                sticky: false
            });
        });
    </script>
<?php endif ?>
<?php if ($this->session->flashdata('success_delete')) : ?>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery.gritter.add({
                title:	'Hapus sukses',
                text:	'Berhasil menghapus data' ,
                image: 	'<?php echo base_url('/asset/backend/img/notif_success.png') ?>',
                sticky: false
            });
        });
    </script>
<?php endif ?>
<?php if ($this->session->flashdata('error')) : ?>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery.gritter.add({
                title:	'Gagal',
                text:	'Inputan salah, cek kembali' ,
                image: 	'<?php echo base_url('/asset/backend/img/notif_error.png') ?>',
                sticky: false
            });
        });
    </script>
<?php endif ?>
<?php if ($this->session->flashdata('error_upload')) : ?>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery.gritter.add({
                title:	'Gagal',
                text:	'Gagal upload gambar',
                image: 	'<?php echo base_url('/asset/backend/img/notif_error.png') ?>',
                sticky: false
            });
        });
    </script>
<?php endif ?>
