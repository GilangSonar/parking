<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo site_url('backend/dashboard')?>">Administrator Pages - Parking System</a>
        </div>
        <div class="navbar-collapse collapse">

            <ul class="nav navbar-nav">
                <li class="<?php if(isset($active_dashboard)){echo $active_dashboard;} ?>"><a href="<?php echo site_url('backend/dashboard')?>">Home</a></li>
                <li class="<?php if(isset($active_data_pelanggar)){echo $active_data_pelanggar;} ?>"><a href="<?php echo site_url('backend/pelanggar')?>">Data Pelanggar</a></li>
                <li class="dropdown <?php if(isset($active_member)){echo $active_member;} ?>">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Data Member <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo site_url('backend/member/daftar_member')?>"><span class="glyphicon glyphicon-user"></span> Daftar Member</a></li>
                        <li><a href="<?php echo site_url('backend/member/deposit_member')?>"><span class="glyphicon glyphicon-barcode"></span> Deposit Member</a></li>
                    </ul>
                </li>
                <li class="<?php if(isset($active_kendaraan)){echo $active_kendaraan;} ?>"><a href="<?php echo site_url('backend/kendaraan')?>">Data Kendaraan</a></li>
            </ul>

            <ul class="nav navbar-nav  navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Admin <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url()?>"><span class="glyphicon glyphicon-log-out"></span> Sign Out</a></li>
                    </ul>
                </li>
            </ul>

        </div><!--/.nav-collapse -->
    </div>
</div>