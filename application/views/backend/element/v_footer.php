<div class="container">
    <div class="row text-center" style="padding-bottom: 20px">
        Parking System - Created By :
        <a href="http://facebook.com/gilang.sonar" target="_blank">Gilang Sonar</a> &
        <a href="http://facebook.com/alkhoholic" target="_blank">Andreas Hirasna</a>
    </div>
</div>

<!-- JS-->
<script type="text/javascript" src="<?php echo base_url('asset/js/jquery.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('asset/js/bootstrap.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('asset/js/dataTables.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('asset/js/datepicker.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('asset/js/jquery.gritter.min.js')?>"></script>

<!--Custom JS-->
<script type="text/javascript" src="<?php echo base_url('asset/js/app.js')?>"></script>

</body>
</html>