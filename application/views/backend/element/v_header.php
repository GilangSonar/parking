<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?php echo base_url('asset/img/logo.png')?>">

    <title><?php echo $title; ?></title>

    <!-- CSS -->
    <link href="<?php echo base_url('asset/css/bootstrap.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('asset/css/bootstrap-theme.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('asset/css/datepicker.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('asset/css/jquery.gritter.css')?>" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url('asset/css/style.css')?>" rel="stylesheet">
</head>

<body>