<?php

class M_member extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    public function getKodeMember()
    {
        $q = $this->db->query("select MAX(RIGHT(id_member,5)) as kd_max from tbl_member");
        $kd = "";
        if($q->num_rows()>0)
        {
            foreach($q->result() as $k)
            {
                $tmp = ((int)$k->kd_max)+1;
                $kd = sprintf("%05s", $tmp);
            }
        }
        else
        {
            $kd = "00001";
        }
        return "MM-".$kd;
    }

    function getAllMember(){
        return $this->db->query("select * from tbl_member")->result();
    }

    public function insertMember($data){
        $query = $this->db->insert('tbl_member',$data);
        return $query;
    }

    public function getByIdMember($id){
        $this->db->where('id_member',$id);
        $query = $this->db->get('tbl_member');
        return $query->result();
    }
}