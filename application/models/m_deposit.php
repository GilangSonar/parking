<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Gilang
 * Date: 10/24/13
 * Time: 11:37 PM
 * To change this template use File | Settings | File Templates.
 */

class M_deposit extends CI_model{
    function __construct(){
        parent::__construct();
    }

    public function getKodeDeposit()
    {
        $q = $this->db->query("select MAX(RIGHT(id_deposit,5)) as kd_max from tbl_deposit");
        $kd = "";
        if($q->num_rows()>0)
        {
            foreach($q->result() as $k)
            {
                $tmp = ((int)$k->kd_max)+1;
                $kd = sprintf("%05s", $tmp);
            }
        }
        else
        {
            $kd = "00001";
        }
        return "DP-".$kd;
    }


    function getAllDeposit(){
        return $this->db->query("select * from tbl_deposit a left join tbl_member b
        on a.id_member=b.id_member order by a.id_deposit ASC")->result();
    }

    public function insertDeposit($data){
        $query = $this->db->insert('tbl_deposit',$data);
        return $query;
    }

    public function updateDeposit($id,$data){
        $this->db->where('kd_barang',$id);
        $update = $this->db->update('tbl_deposit',$data);
        return $update;
    }

    public function deleteDeposit($id){
        $this->db->where('kd_barang',$id);
        $delete = $this->db->delete('tbl_deposit');
        return $delete;
    }
}