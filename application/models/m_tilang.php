<?php

class M_tilang extends CI_Model{
    function __construct(){
        parent::__construct();
    }


//     ========= Model Contact Info =============== //
    public function getKodeTilang(){
        $q = $this->db->query("select MAX(RIGHT(id_pelanggaran,5)) as kd_max from tbl_pelanggaran");
        $kd = "";
        if($q->num_rows()>0)
        {
            foreach($q->result() as $k)
            {
                $tmp = ((int)$k->kd_max)+1;
                $kd = sprintf("%05s", $tmp);
            }
        }
        else
        {
            $kd = "00001";
        }
        return "ST-".$kd;
    }

    function getAllPelanggaran(){
        return $this->db->query("select * from tbl_pelanggaran")->result();
    }

    function insertPelanggaran($data){
        $query = $this->db->insert('tbl_pelanggaran',$data);
        return $query;
    }

    function updatePelanggaran($id,$data){
        $this->db->where('id_pelanggaran',$id);
        $update = $this->db->update('tbl_pelanggaran',$data);
        return $update;
    }

    function deletePelanggaran($id){
        $this->db->where('id_pelanggaran',$id);
        $delete = $this->db->delete('tbl_pelanggaran');
        return $delete;
    }

    function getDataPelanggar($id){
        $this->db->select('*');
        $this->db->from('tbl_kendaraan a');
        $this->db->join('tbl_member b','a.id_pemilik = b.id_member','left');
        $this->db->where('a.id_pemilik',$id);
        $q = $this->db->get();
        return $q->result();
    }
}